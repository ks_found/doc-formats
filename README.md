# doc-formats

_Wiki page: https://gitlab.com/ks_found/protocols/wikis/Article-Media-Type_

Webpage demo of rendering various document formats. The `sample.odt` document is the source and contains text as well as non-text embedded items.

At present this tests:

* ODT rendering - [webodf](https://webodf.org/)
* PDF rendering - [PDF.js](https://mozilla.github.io/pdf.js/)

## Online demo

https://ks_found.gitlab.io/doc-formats/

## Run locally

Prerequisites:

* [Node.js](https://nodejs.org/) - latest stable release
* A modern browser (I've only tested in Chrome so far!)

To run the demo go into the folder and run:

```shell
npx serve public
```

The demo website should now be running at http://localhost:5000 _(check the output of the command above to confirm)_.

## License

MIT